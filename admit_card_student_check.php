<!DOCTYPE html>
<?php
 include 'dbconfig.php';
?>
<?php include 'logout_header.php';?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="index.css">
</head>
<body>
    <div class="profile_body">
        <div class="profile_body_left">
            <h1>WELCOME TO WUB</h1>
        </div>
        <div class="profile_body_right">
        </div>
        <div class="profile_body_down">
            <form action="admit_show.php" method="POST">
           <table class="profile_table">
              <tr>
                  <td>Department:</td>
                  <td>
                      <select name="department" required="1">
                          <option value="">Select</option>
                          <?php 
                            $select=mysql_query("SELECT DISTINCT std_dpt FROM student_info");
                            while($fetch=mysql_fetch_array($select))
                            {
                          ?>
                          
                          <option value="<?php echo $fetch['std_dpt']; ?>"><?php echo $fetch['std_dpt']; ?></option>
                          <?php }?>
                      </select>
                  </td>
              </tr>
              <tr>
                  <td>Roll:</td>
                  <td>
                      <select name="roll" required="1">
                          <option value="">Select</option>
                          <?php 
                            $select=mysql_query("SELECT DISTINCT roll FROM student_info");
                            while($fetch=mysql_fetch_array($select))
                            {
                          ?>
                          <option value="<?php echo $fetch['roll']; ?>"><?php echo $fetch['roll']; ?></option>
                          <?php }?>
                      </select>
                  </td>
              </tr>
              
               <tr>
                  <td>Semester:</td>
                  <td>
                      <select name="semester" required="1">
                          <option value="">Select</option>
                          <option value="1st">1st</option>
                          <option value="2nd">2nd</option>
                          <option value="3rd">3rd</option>
                          <option value="4th">4th</option>
                          <option value="5th">5th</option>
                          <option value="6th">6th</option>
                          <option value="7th">7th</option>
                          <option value="8th">8th</option>
                          <option value="9th">9th</option>
                          <option value="10th">10th</option>
                          <option value="11th">11th</option>
                          <option value="12th">12th</option>
                      </select>
                  </td>
              </tr>
               <tr>
                  <td>Exam Name:</td>
                  <td>
                      <select name="exam_name" required="1">
                          <option value="">Select</option>
                          <option value="Class_test">Class Test</option>
                          <option value="Mid_term">Mid-Term</option>
                          <option value="Final">Final</option>
                      </select>
                  </td>
              </tr>
               <tr>
                  <td>Exam Start Date:</td>
                  <td>
                     <input type="date" name="sdate" required="1">
                  </td>
              </tr>
               <tr>
                  <td>Exam End Date:</td>
                  <td>
                     <input type="date" name="edate" required="1">
                  </td>
              </tr>
              <tr>
                  <td></td>
                  <td><input type="submit" name="submit" value="Check"></td>
              </tr>
           </table>
           </form>
            
        </div>
        
    </div>
    
    <div class="foot"> <?php include 'footer.php';?></div>
</body>
</html>