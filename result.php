<?php
 include 'dbconfig.php';
?>
<?php include 'header.php';?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="index.css">
</head>
<body>
    <div class="profile_body">
        <div class="profile_body_left" style="width: 100%;">
           <h1>WELCOME TO WORLD UNIVERSITY OF BANGLADESH</h1>
        </div>
        
        <div class="profile_body_down" style="overflow: hidden;">
            <h3>Check Your Result</h3>
            <form action="result_check.php" method="POST">
           <table class="profile_table">
              <tr>
                  <td>Department:</td>
                  <td>
                      <select name="department" required="1">
                          <option value="">----Please Select----</option>
                          <option value="CSE">CSE</option>
                          <option value="BBA">BBA</option>
                          <option value="EEE">EEE</option>
                          <option value="CEVIL">CEVIL</option>
                          <option value="Mecattronics">Mecattronics</option>
                      </select>
                  </td>
              </tr>
              <tr>
                  <td>Roll:</td>
                  <td><input type="text" name="roll" required="1"></td>
              </tr>
              <tr>
                  <td></td>
                  <td><input type="submit" name="submit" value="Check"></td>
              </tr>
           </table>
           </form>
        </div>
        
    </div>
    
    <div class="foot"> <?php include 'footer.php';?></div>
</body>
</html>