<?php 

 include 'header.php';
 ?>

<html>
    <head>
        <title>Welcome to World University of Bangladesh </title>
        <link rel="stylesheet" type="text/css" href="form.css">
    </head>
    <body>
        <div class="body_form">
           
         <div class="form_right"></div>
            <div class="form_left">
                <b>Form Number:</b>2<br><br>
                <b>Student Information:</b>
        <table>
            <tr>
                <td>Full Name: </td>
                <td><input type="text" class="parents" name="std_name"> <font size="2" color="red">As Mark sheet</font> </td>
            </tr>
            <tr>
                <td>Date Of Birth: </td>
                <td><input type="Date" class="parents" name="DOB"> </td>
            </tr>
            <tr>
                <td>Nationality: </td>
                <td>
                    <select name="nationality">
                        <option value="Bangladeshi"> Bangladeshi </option>
                    </select>
                    &nbsp; &nbsp; Blood Group: <input type="text" style="width:35px;">
                </td>
            </tr>
            <tr>
                <td>Sex: </td>
                <td><input type="radio" name="1"> Male
                    <input type="radio" name="1"> Female
                    <input type="radio" name="1"> Others</td>
            </tr>
            <tr>
                <td>Marital Status: </td>
                <td><input type="radio" name="2"> Married
                <input type="radio" name="2"> Unmarried
                <input type="radio" name="2"> Divorce</td>
            </tr>
            
            <tr>
                <td>Present Address: </td>
                <td>
                    <textarea></textarea>
                </td>
            </tr>
            <tr>
                <td>Permanent Address: </td>
                <td>
                    <textarea></textarea>
                </td>
            </tr>
            <tr>
                <td>Phone Number: </td>
                <td>
                    <input type="number" class="parents" name="phone" required="1"> <font size="2" color="red">With Country Code</font>
                </td>
            </tr>
            <tr>
                <td>Email: </td>
                <td>
                    <input type="email" class="parents" name="phone" required="1">
                </td>
            </tr>
            <tr>
                <td>Proposed Program of study at WUB: </td>
                <td>
                    <select>
                        <option>Select Course</option>
                        <option>CSE</option>
                        <option>BBA</option>
                        <option>EEE</option>
                        <option>LLB</option>
                        <option>Mecattronics</option>
                        <option>CEVIL</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>How did you first hear of WUB:</td>
                <td>
                    <input type="checkbox">Newspaper
                    <input type="checkbox">Friend/Relative
                    <input type="checkbox">Website
                    <input type="checkbox">Email ad
                    <input type="checkbox">Banner
                    <input type="checkbox">Others
                </td>
            </tr>
        
                <tr><td colspan="2"><b>Parents Information:</b></td></tr>
               
                    <tr>
                        <td>Father's Name:</td>
                        <td><input type="text" class="parents"></td>
                    </tr>
                    <tr>
                        <td>Ocapation:</td>
                        <td><input type="text" class="parents"></td>
                    </tr>
                   
                    
                </table>
    </div>
    <div class="form_line1"></div>
    <div class="form_right"></div>
    <div class="form_mid">
        <table>
             <tr>
                        <td>Father's Address:</td>
                        <td><textarea class="parents" style="height: 30px;"></textarea></td>
                        
                    </tr>
             <tr>
                        <td>Mothers's Name:</td>
                        <td><input type="text" class="parents"></td>
                        <td>Ocapation:</td>
                        <td><input type="text" class="parents"></td>
               </tr>
               <tr>
                   <td>Address:</td>
                   <td><textarea class="parents"></textarea></td>
               </tr>
        </table>
        <b>Educational Qualification:</b>
        <table border="1" cellspacing="0" style="text-align: center;">
            <tr>
                <td>Name Of Exam</td>
                <td>Year</td>
                <td>Group</td>
                <td>Institute</td>
                <td>Board</td>
                <td>Result</td>
                <td>Major</td>
            </tr>
            <tr>
                <td>
                    <select name="exam_name" class="form_input1" required="1">
                        <option value="">Select</option>
                        <option value="SSC">SSC</option>
                        <option value="HSC">HSC</option>
                        <option value="Graduation">Graduation</option>
                    </select>
                </td>
                <td><input type="text" class="form_input" name="year" required="1"></td>
                <td>
                    <select class="form_input1" name="group" required="1">
                        <option value="">Select</option>
                        <option value="Science">Science</option>
                        <option value="General">General</option>
                        <option value="Commerce">Commerce</option>
                        <option value="BSC">BSC</option>
                        <option value="LLB">LLB</option>
                        <option value="BBA">BBA</option>
                    </select>
                </td>
                <td><input type="text" name="institute" class="form_input2" required="1"></td>
                <td>
                    <select class="form_input1" name="board" required="1">
                        <option value="">Select</option>
                        <option value="Dhaka">Dhaka</option>
                        <option value="Chittagong">Chittagong</option>
                        <option value="Rajsahi">Rajsahi</option>
                        <option value="Comilla">Comilla</option>
                        <option value="Rangpur">Rongpur</option>
                        <option value="Technical">Technical</option>
                        <option value="Madtasah">Madrasah</option>
                        <option value="University">University</option>
                    </select>
                </td>
                <td><input type="text" class="form_input" name="result" required="1" ></td>
                <td> 
                  <select class="form_input1" name="major" required="1">
                      <option value="">Select</option>
                      <option value="Math">Math</option>
                      <option value="Biology">Biology</option>
                      <option value="Computer">Computer</option>
                      <option value="Agriculture">Agriculture</option>
                  </select>
                </td>
            </tr>
            <tr>
                <td>
                    <select name="exam_name" class="form_input1" required="1">
                        <option value="">Select</option>
                        <option value="SSC">SSC</option>
                        <option value="HSC">HSC</option>
                        <option value="Graduation">Graduation</option>
                    </select>
                </td>
                <td><input type="text" class="form_input" name="year" required="1"></td>
                <td>
                    <select class="form_input1" name="group" required="1">
                        <option value="">Select</option>
                        <option value="Science">Science</option>
                        <option value="General">General</option>
                        <option value="Commerce">Commerce</option>
                        <option value="BSC">BSC</option>
                        <option value="LLB">LLB</option>
                        <option value="BBA">BBA</option>
                    </select>
                </td>
                <td><input type="text" name="institute" class="form_input2" required="1"></td>
                <td>
                    <select class="form_input1" name="board" required="1">
                        <option value="">Select</option>
                        <option value="Dhaka">Dhaka</option>
                        <option value="Chittagong">Chittagong</option>
                        <option value="Rajsahi">Rajsahi</option>
                        <option value="Comilla">Comilla</option>
                        <option value="Rangpur">Rongpur</option>
                        <option value="Technical">Technical</option>
                        <option value="Madtasah">Madrasah</option>
                    </select>
                </td>
                <td><input type="text" class="form_input" name="result" required="1" ></td>
                <td> 
                  <select class="form_input1" name="major" required="1">
                      <option value="">Select</option>
                      <option value="Math">Math</option>
                      <option value="Biology">Biology</option>
                      <option value="Computer">Computer</option>
                      <option value="Agriculture">Agriculture</option>
                  </select>
                </td>
            </tr>
            <tr>
                <td>
                    <select name="exam_name" class="form_input1" required="1">
                        <option value="">Select</option>
                        <option value="SSC">SSC</option>
                        <option value="HSC">HSC</option>
                        <option value="Graduation">Graduation</option>
                    </select>
                </td>
                <td><input type="text" class="form_input" name="year" required="1"></td>
                <td>
                    <select class="form_input1" name="group" required="1">
                        <option value="">Select</option>
                        <option value="Science">Science</option>
                        <option value="General">General</option>
                        <option value="Commerce">Commerce</option>
                        <option value="BSC">BSC</option>
                        <option value="LLB">LLB</option>
                        <option value="BBA">BBA</option>
                    </select>
                </td>
                <td><input type="text" name="institute" class="form_input2" required="1"></td>
                <td>
                    <select class="form_input1" name="board" required="1">
                        <option value="">Select</option>
                        <option value="Dhaka">Dhaka</option>
                        <option value="Chittagong">Chittagong</option>
                        <option value="Rajsahi">Rajsahi</option>
                        <option value="Comilla">Comilla</option>
                        <option value="Rangpur">Rongpur</option>
                        <option value="Technical">Technical</option>
                        <option value="Madtasah">Madrasah</option>
                    </select>
                </td>
                <td><input type="text" class="form_input" name="result" required="1" ></td>
                <td> 
                  <select class="form_input1" name="major" required="1">
                      <option value="">Select</option>
                      <option value="Math">Math</option>
                      <option value="Biology">Biology</option>
                      <option value="Computer">Computer</option>
                      <option value="Agriculture">Agriculture</option>
                  </select>
                </td>
            </tr>
        </table>
        <b>Image Upload</b><br>
        <input type="file" name="file" required="1" style="border: 1px solid gray;"><br><br>
        <b>Payment Type</b>
        <select name="payment" required="1">
            <option value="">----Select----</option>
            <option value="Bkash">Bkash</option>
            <option value="By Hand">By Hand</option>
        </select><br><br>
        <b>Bkash Payment Number: </b>01670514306<br><br>
        <b>Payment Id</b>
        <input type="text" name="payment_id" required="1"><font size="2"> Bkash TrxID or Money receipt number</font>
        <br><br>
        <b><a href="images/bkash.JPG" target="_blank">Payment Method</a></b>
        <br><br>
        <input type="checkbox" required="1" style="width: 18px; height: 18px; float: left;">
        I agree with this information<br>
        
        <div style="width: 100%; height: 30px; text-align: right;"><input type="submit" value="Submit" name="submit"></div>
    </div>
   
    
            
        </div>
        <div class="foot_form">
           <?php include 'footer.php'; ?> 
        </div>
    </body>
    
</html>
