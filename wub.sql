-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 14, 2015 at 04:54 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wub`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
`ac_id` int(11) NOT NULL,
  `std_id` varchar(100) NOT NULL,
  `staff_id` varchar(100) NOT NULL,
  `have_to_pay` varchar(300) NOT NULL,
  `total_paid` varchar(300) NOT NULL,
  `last_paid` varchar(300) NOT NULL,
  `due` varchar(300) NOT NULL,
  `recept_number` varchar(200) NOT NULL,
  `payment_type` varchar(200) NOT NULL,
  `money_recept` varchar(300) NOT NULL,
  `pay_date` varchar(300) NOT NULL,
  `paid_time` varchar(300) NOT NULL,
  `clearence` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`ac_id`, `std_id`, `staff_id`, `have_to_pay`, `total_paid`, `last_paid`, `due`, `recept_number`, `payment_type`, `money_recept`, `pay_date`, `paid_time`, `clearence`) VALUES
(1, '1', '2', '336000', '0', '0', '336000', '', '', '', '10/12/2015', '10:20:20', ''),
(2, '2', '2', '3600', '200', '200', '3400', '1365', 'gsdhfgdfh', '14460978442 tk.jpg', 'Thursday, October 29, 2015', '1446097844', ''),
(11, '', '2', '3600', '400', '200', '3200', '1365', 'gsdhfgdfh', '14461086292 tk.jpg', 'Thursday, October 29, 2015', '1446108629', ''),
(12, '2', '2', '3600', '400', '200', '3200', '1365', 'gsdhfgdfh', '14461094722 tk.jpg', 'Thursday, October 29, 2015', '1446109472', ''),
(13, '2', '2', '3600', '600', '200', '3000', '1365', 'gsdhfgdfh', '14461095292 tk.jpg', 'Thursday, October 29, 2015', '1446109529', ''),
(14, '2', '2', '3600', '800', '200', '2800', '1365', 'gsdhfgdfh', '14461096802 tk.jpg', 'Thursday, October 29, 2015', '1446109680', ''),
(15, '1', '2', '336000', '3000', '3000', '333000', '13698', 'Bill', '14461097985675.jpg', 'Thursday, October 29, 2015', '1446109798', ''),
(16, '1', '2', '336000', '6000', '3000', '330000', '1365', 'gsdhfgdfh', '14461113632 tk.jpg', '29/10/15', '1446111363', ''),
(17, '1', '2', '336000', '6000', '3000', '330000', '1365', 'gsdhfgdfh', '14461119452 tk.jpg', '29/10/15', '03:45:45pm', ''),
(18, '1', '2', '336000', '9000', '3000', '327000', '13698', 'gsdhfgdfh', '14461119722 tk.jpg', '29/10/15', '03:46:12pm', ''),
(19, '1', '2', '336000', '9000', '', '327000', '', '', '1446127609', '29/10/15', '08:06:49pm', ''),
(20, '1', '2', '336000', '12000', '3000', '324000', '16896', 'Monthly Payment', '14461283312 tk.jpg', '29/10/15', '08:18:51pm', ''),
(21, '1', '2', '336000', '12000', '3000', '324000', '16896', 'Monthly Payment', '14461283382 tk.jpg', '29/10/15', '08:18:58pm', ''),
(22, '1', '2', '336000', '15000', '3000', '321000', '159874', 'Monthly Payment', '144696310212477_532894516828489_84424095_n.jpg', '08/11/15', '12:11:42pm', 'Yes'),
(23, '1', '2', '336000', '18000', '3000', '318000', '159874', 'Monthly Payment', '14469633445675.jpg', '08/11/15', '12:15:44pm', 'Yes'),
(24, '1', '2', '336000', '21000', '3000', '315000', '159874', 'Monthly Payment', '144696342873541_317279061742412_320027578_n.jpg', '08/11/15', '12:17:08pm', 'Yes'),
(25, '1', '2', '336000', '24000', '3000', '312000', '159874', 'Monthly Payment', '14469634775675.jpg', '08/11/15', '12:17:57pm', 'Yes'),
(26, '1', '2', '336000', '27000', '3000', '309000', '5687', 'Monthly Payment', '14473548242 tk1.jpg', '12/11/15', '01:00:24am', 'Yes'),
(27, '1', '2', '336000', '30000', '3000', '306000', '5687', 'Monthly Payment', '14473549135675.jpg', '12/11/15', '01:01:53am', 'No'),
(28, '1', '2', '336000', '33000', '3000', '303000', '5687', 'Monthly Payment', '14473549262 tk1.jpg', '12/11/15', '01:02:06am', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `ac_staff`
--

CREATE TABLE IF NOT EXISTS `ac_staff` (
`staff_id` int(11) NOT NULL,
  `staff_name` varchar(300) NOT NULL,
  `occupation` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `date_of_birth` varchar(300) NOT NULL,
  `join_date` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ac_staff`
--

INSERT INTO `ac_staff` (`staff_id`, `staff_name`, `occupation`, `email`, `password`, `date_of_birth`, `join_date`) VALUES
(1, 'Abdul Kuddus', 'Senior Accounts', 'kuddus@gmail.com', 'kuddus', '11/15/1993', '11/12/2013'),
(2, 'Abdullah', 'Junior Accounts', 'abdullah@gmail.com', 'abdullah', '1/1/1990', '10/12/2010');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `present_address` varchar(300) NOT NULL,
  `permanent_address` varchar(300) NOT NULL,
  `gender` varchar(15) NOT NULL,
  `date_of_birth` varchar(15) NOT NULL,
  `join_date` varchar(15) NOT NULL,
  `position` varchar(300) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `answar1` varchar(50) NOT NULL,
  `answar2` varchar(50) NOT NULL,
  `image` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `present_address`, `permanent_address`, `gender`, `date_of_birth`, `join_date`, `position`, `email`, `password`, `answar1`, `answar2`, `image`) VALUES
(1, 'Mayeen Uddin', '165/D, Tejkunipara, Tejgaon, Dhaka-1215', '165/D, Tejkunipara, Tejgaon, Dhaka-1215', 'Male', '15/11/1993', '01/01/15', 'Senior Teacher', 'mayeennbd@gmail.com', 'mA6kash', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
`course_id` int(11) NOT NULL,
  `c_code` varchar(100) NOT NULL,
  `c_name` varchar(100) NOT NULL,
  `credit` varchar(11) NOT NULL,
  `department` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`course_id`, `c_code`, `c_name`, `credit`, `department`) VALUES
(1, 'CSE101', 'Fundamental Of computing', '3', 'CSE'),
(2, 'CSE102', 'Web Development', '3', 'CSE'),
(3, 'BBA105', 'Fundamental Of Business', '3', 'BBA');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
`department_id` int(11) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `note` varchar(300) NOT NULL,
  `cost` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`department_id`, `department_name`, `note`, `cost`) VALUES
(1, 'CSE', 'Part of science', '400000'),
(2, 'BBA', 'Part of Business', '350000'),
(3, 'EEE', 'Part of science', '400000'),
(4, 'Low', 'Part of Low', '38000');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
`file_id` int(11) NOT NULL,
  `id` varchar(300) NOT NULL,
  `data` varchar(300) NOT NULL,
  `upload_date` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file`
--

INSERT INTO `file` (`file_id`, `id`, `data`, `upload_date`) VALUES
(67, '5', 'home.php', 'Friday, October 23, 2015'),
(109, '3', 'db_config.php', 'Friday, October 23, 2015'),
(116, '1', '14457841472 tk.jpg', 'Sunday, October 25, 2015'),
(119, '2', '1446719719IMG_0001_NEW01.jpg', 'Thursday, November 5, 2015'),
(120, '1', '14470054442 tk1.jpg', 'Sunday, November 8, 2015'),
(121, '1', '1447005472198299_390303567714495_1785956104_n.jpg', 'Sunday, November 8, 2015');

-- --------------------------------------------------------

--
-- Table structure for table `registered_course`
--

CREATE TABLE IF NOT EXISTS `registered_course` (
`sub_id` int(11) NOT NULL,
  `std_id` varchar(50) NOT NULL,
  `course_id` varchar(50) NOT NULL,
  `semester` varchar(50) NOT NULL,
  `note` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registered_course`
--

INSERT INTO `registered_course` (`sub_id`, `std_id`, `course_id`, `semester`, `note`) VALUES
(1, '1', '1', '1st', 'Fundamental'),
(2, '1', '2', '1st', 'HTML,CSS'),
(3, '2', '1', '1st', 'Fundamental'),
(4, '1', '3', '2nd', 'Fundamental of Business');

-- --------------------------------------------------------

--
-- Table structure for table `score_table`
--

CREATE TABLE IF NOT EXISTS `score_table` (
`score_id` int(11) NOT NULL,
  `teacher_id` varchar(50) NOT NULL,
  `std_id` varchar(100) NOT NULL,
  `course_id` varchar(100) NOT NULL,
  `class_test` varchar(100) NOT NULL,
  `mid_term` varchar(100) NOT NULL,
  `final` varchar(100) NOT NULL,
  `total` varchar(50) NOT NULL,
  `result` varchar(100) NOT NULL,
  `point` varchar(100) NOT NULL,
  `note` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `score_table`
--

INSERT INTO `score_table` (`score_id`, `teacher_id`, `std_id`, `course_id`, `class_test`, `mid_term`, `final`, `total`, `result`, `point`, `note`) VALUES
(1, '1', '1', '1', '15', '15', '30', '60', 'B+', '3.35', 'Good'),
(2, '1', '1', '3', '15', '15', '30', '60', 'A-', '3.35', 'Good');

-- --------------------------------------------------------

--
-- Table structure for table `student_info`
--

CREATE TABLE IF NOT EXISTS `student_info` (
`std_id` int(11) NOT NULL,
  `name` varchar(300) NOT NULL,
  `roll` varchar(300) NOT NULL,
  `reg` varchar(300) NOT NULL,
  `std_dpt` varchar(300) NOT NULL,
  `batch` varchar(20) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `pre_add` varchar(300) NOT NULL,
  `parmanent_add` varchar(300) NOT NULL,
  `date_of_birth` varchar(300) NOT NULL,
  `gender` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_info`
--

INSERT INTO `student_info` (`std_id`, `name`, `roll`, `reg`, `std_dpt`, `batch`, `email`, `password`, `pre_add`, `parmanent_add`, `date_of_birth`, `gender`) VALUES
(1, 'Mayeen Uddin', '659', 'WUB 03/12/27/659', 'CSE', '27(B)', 'mohinnbd@gmail.com', 'mA6kash', '165/D,Tejkunipara,Tejgaon,Dhaka-1215', 'Abdullah Pur, Begumgonje, Noakhali', '15/11/1993', 'Male'),
(2, 'Aziz', '655', 'WUB03/12/27/655', 'CSE', '27(B)', 'azizul.rokib@gmail.com', 'aziz', 'Dhaka', 'Pabna', '14/12/1993', 'Male');

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE IF NOT EXISTS `teacher` (
`id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `dpt` varchar(30) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id`, `name`, `dpt`, `email`, `password`) VALUES
(1, 'Kazi Hasan Robin', 'CSE', 'khr@gmail.com', 'khrobin'),
(2, 'Mohammad Sultan Mahmud', 'CSE', 'm.smahmud@yahoo.com', 'm.smahmud'),
(3, 'Sohidul islam sumon', 'EEE', 's.sumon@gmail.com', 's.sumon'),
(4, 'Ashiqur Rahman', 'CSE', 'ashiq@gmail.com', 'a.shiq'),
(5, 'Anisurrahman', 'BBA', 'anis@gmail.com', 'a.nis');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
 ADD PRIMARY KEY (`ac_id`);

--
-- Indexes for table `ac_staff`
--
ALTER TABLE `ac_staff`
 ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
 ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
 ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
 ADD KEY `file_id` (`file_id`);

--
-- Indexes for table `registered_course`
--
ALTER TABLE `registered_course`
 ADD PRIMARY KEY (`sub_id`);

--
-- Indexes for table `score_table`
--
ALTER TABLE `score_table`
 ADD PRIMARY KEY (`score_id`);

--
-- Indexes for table `student_info`
--
ALTER TABLE `student_info`
 ADD PRIMARY KEY (`std_id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
MODIFY `ac_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `ac_staff`
--
ALTER TABLE `ac_staff`
MODIFY `staff_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT for table `registered_course`
--
ALTER TABLE `registered_course`
MODIFY `sub_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `score_table`
--
ALTER TABLE `score_table`
MODIFY `score_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `student_info`
--
ALTER TABLE `student_info`
MODIFY `std_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
