<?php 
 session_start();
 include 'dbconfig.php';
 if (!$_SESSION['std_id'])
 {
     header("location: login.php");
 }

?>
   <?php 
            $std_id=$_SESSION['std_id'];
           $select=mysql_query("SELECT * FROM student_info WHERE std_id='$std_id'");
            $fetch=mysql_fetch_array($select);
           $std_dpt=$fetch['std_dpt'];
    ?>
      
<html>
<head>
    <title>Result Profile</title>
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" type="text/css" href="header.css">
</head>
<body>
    <!profile header Start>
    
    <?php
    include 'logout_header.php';
    
    ?>
    <!profile header close>
    <?php 
               $select1=mysql_query("SELECT * FROM account WHERE std_id='$std_id' ORDER BY ac_id DESC");
              $fetch1=mysql_fetch_array($select1);
           
              ?>
   
    <div class="profile_body">
        <div class="profile_body_left">
            <h3><?php echo $fetch['name']?> </h3>
            <h4> Department Of <?php echo $std_dpt;?> </h4>
               
        </div>
        <div class="profile_body_right">
            <p class="p"><a href="result_details.php">Details</a></p>
        </div>
        <div class="profile_body_down">
           
               Your Details:  
            <table class="profile_table">
              <tr>
                 <td>Roll:</td>
                 <td><?php echo $fetch['roll'];?></td>
              </tr>
              <tr>
                 <td>Registration:</td>
                 <td><?php echo $fetch['reg'];?></td>
              </tr>
              <tr>
                 <td>Date Of Birth:</td>
                 <td><?php echo $fetch['date_of_birth'];?></td>
              </tr>
               <?php 
                     $select2=mysql_query("SELECT * FROM registered_course WHERE std_id='$std_id' ORDER BY sub_id DESC");
                     $fetch2=mysql_fetch_array($select2);
                   ?>
              <tr>
                 <td>Your Semester:</td>
                 <td><?php echo $semester=$fetch2['semester'];?></td>
              </tr>
            </table>
               <table style="width: 95%; text-align: center;">
                    <tr style="background: seagreen;">
                       <td>Serial</td>
                       <td>Student ID</td>
                       <td>Subject ID</td>
                       <td>Subject Name</td>
                       <td>Subject Code</td>
                       <td>Mid Term</td>
                       <td>Class Test</td>
                       <td>Final</td>
                       <td>Total</td>
                       <td>Result</td>
                       <td>Point</td>
                   </tr>
                   <?php 
                   $sub_id=$fetch2['sub_id'];
                     $select3=mysql_query("SELECT registered_course.*,score_table.* FROM registered_course 
                                           INNER JOIN score_table 
                                           ON registered_course.sub_id=score_table.sub_id && score_table.std_id='$std_id'
                                            && semester='$semester'");
                     $i=0;
                     while($fetch3=mysql_fetch_array($select3)){
                         $course_id=$fetch3['course_id'];
                         $select4=mysql_query("SELECT * FROM course WHERE course_id='$course_id'");
                         $fetch4=mysql_fetch_array($select4);
                       $i++;
                        $color=($i%2==0)?"lightblue":"white";
                   ?>
                   <tr bgcolor="<?php echo $color?>">
                       <td><?php echo $i;?></td>
                       <td><?php echo $fetch3['std_id']; ?></td>
                       <td><?php echo $fetch3['sub_id']; ?></td>
                       <td><?php echo $fetch4['c_name']; ?></td>
                       <td><?php echo $fetch4['c_code']; ?></td>
                       <td><?php echo $mid=$fetch3['mid_term']; ?></td>
                       <td><?php echo $class_test=$fetch3['class_test']; ?></td>
                       <td><?php echo $final=$fetch3['final']; ?></td>
                       <?php $total=$mid+$class_test+$final; ?>
                       <td><?php echo $total; ?></td>
                       <td><?php echo $result=$fetch3['result']; ?></td>
                       <td><?php echo $point=$fetch3['point']; ?></td>
                   </tr>
                   <?php  }?>
               </table>
            
        </div>
        
    </div>
    
    <div class="foot"> <?php include 'footer.php';?></div>
</body>
</html>