<?php 
 session_start();
 include 'dbconfig.php';
 if (!$_SESSION['id'])
 {
     header("location: login.php");
 }

?>
   <?php 
            $id=$_SESSION['id'];
            if(isset($_POST['submit']))
            {
                $std_dpt=$_POST['std_dpt'];
                $batch=$_POST['batch'];
                $roll=$_POST['roll'];
                $semester=$_POST['semester'];
                  $select=mysql_query("SELECT * FROM student_info WHERE std_dpt='$std_dpt' && batch='$batch' && roll='$roll'");
                  $fetch=mysql_fetch_array($select);
                  $std_id=$fetch['std_id'];
            }
    ?>
      
<html>
<head>
    <title>Profile</title>
    <link rel="stylesheet" type="text/css" href="index.css">
    <link rel="stylesheet" type="text/css" href="header.css">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript">
        function show(course_code){
            $('.c_name').load('get_course_name.php?c_code='+course_code);
            $('.credit').load('get_course_credit.php?c_code='+course_code);
        }
    </script>
</head>
<body>
    <!profile header Start>
    
        <?php 
             include 'logout_header.php';
        ?>
    <!profile header close>
    
    <div class="profile_body">
        <div class="profile_body_left">
            <h3>This Student Name: <?php echo $fetch['name']?> </h3>
            <h4> Department:<?php echo $fetch['std_dpt']?> </h4>
               
        </div>
        <div class="profile_body_right">
           <p class="p"><a href="input_result_details.php">Details</a></p>
        </div>
        <div class="profile_body_down">
            <form action="result_input_check.php" method="POST">
            <table class="profile_table1">
                <tr>
                    <td>Course Code</td>
                    <td>Course Name</td>
                    <td>Course Credit</td>
                    <td>Class Test</td>
                    <td>Mid-Term</td>
                    <td>Final</td>
                    <td>Point</td>
                    <td>Result</td>
                    <td>Note</td>
                </tr>
            <?php 
              
               $select1=mysql_query("SELECT * FROM registered_course WHERE std_id='$std_id' && semester='$semester'"); 
               ?>
               <tr>
                   <td>
                       <select name="c_code" onchange="show(this.value)" required="1">
                           
                           <option value="">----Select----</option>
                             <?php 
                             while($fetch1=mysql_fetch_array($select1))
                             {
                              $course_id=$fetch1['course_id'];
                              $select2=mysql_query("SELECT * FROM course WHERE course_id='$course_id'");
                              $fetch2=mysql_fetch_array($select2);
                 
                             ?>
                            <option value="<?php echo $fetch2['c_code'];?>"><?php echo $fetch2['c_code'];?></option>
                           <?php }?>
                       </select>
                   </td>
                   <td class="c_name">No Subject</td>
                   <td class="credit">No Credit</td>
                   <td><input type="text" name="class_test" style="width: 50px;" required="1"></td>
                   <td><input type="text" name="mid_term" style="width: 50px;" required="1"></td>
                   <td><input type="text" name="final" style="width: 50px;" required="1"></td>
                   <td><input type="text" name="point" style="width: 50px;" required="1"></td>
                   <td>
                       <select name="result" required="1">
                           <option value="">---Select---</option>
                           <option value="A+">A+</option>
                           <option value="A">A</option>
                           <option value="A-">A-</option>
                           <option value="B+">B+</option>
                           <option value="B">B</option>
                           <option value="B-">B-</option>
                           <option value="C+">C+</option>
                           <option value="C">C</option>
                           <option value="D">D</option>
                       </select>
                   </td>
                   <td>
                       <select name="note" required="1">
                           <option value="">---Select---</option>
                           <option value="Good">Good</option>
                           <option value="Batter">Batter</option>
                           <option value="Best">Best</option>
                       </select>
                   </td>
               </tr>
              
                
            </table>
            <input type="submit" value="Submit" name="insert">
             </form>
            <?php $_SESSION['std_id']=$std_id;?>
        </div>
        
    </div>
    
    <div class="foot"> <?php include 'footer.php';?></div>
</body>
</html>